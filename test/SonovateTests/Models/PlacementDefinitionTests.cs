using Sonovate.Models;
using System;
using System.Linq;
using Xunit;

namespace SonovateTests
{
    public class PlacementDefinitionTests
	{
		[Fact]
		public void DateRanges_GeneratesCorrectRanges_Weekly1()
		{
			var definition = new PlacementDefinition
			{
				Start = new DateTime(2017,01,02),
				End = new DateTime(2017,02,05),
				PlacementType = PlacementType.Weekly
			};

			var result = definition.TimesheetDateRanges;

			Assert.NotNull(result);
			Assert.Equal(5, result.Count());

			Assert.Equal(new DateTime(2017,01,02), result.First().Start);
			Assert.Equal(new DateTime(2017,01,08), result.First().End);

			Assert.Equal(new DateTime(2017,01,09), result.Skip(1).First().Start);
			Assert.Equal(new DateTime(2017,01,15), result.Skip(1).First().End);

			Assert.Equal(new DateTime(2017,01,16), result.Skip(2).First().Start);
			Assert.Equal(new DateTime(2017,01,22), result.Skip(2).First().End);

			Assert.Equal(new DateTime(2017,01,23), result.Skip(3).First().Start);
			Assert.Equal(new DateTime(2017,01,29), result.Skip(3).First().End);

			Assert.Equal(new DateTime(2017,01,30), result.Skip(4).First().Start);
			Assert.Equal(new DateTime(2017,02,05), result.Skip(4).First().End);
		}

		[Fact]
		public void DateRanges_GeneratesCorrectRanges_Weekly2()
		{
			var definition = new PlacementDefinition
			{
				Start = new DateTime(2017,01,04),
				End = new DateTime(2017,01,12),
				PlacementType = PlacementType.Weekly
			};

			var result = definition.TimesheetDateRanges;

			Assert.NotNull(result);
			Assert.Equal(2, result.Count());

			Assert.Equal(new DateTime(2017,01,04), result.First().Start);
			Assert.Equal(new DateTime(2017,01,08), result.First().End);

			Assert.Equal(new DateTime(2017,01,09), result.Skip(1).First().Start);
			Assert.Equal(new DateTime(2017,01,12), result.Skip(1).First().End);
		}

		[Fact]
		public void DateRanges_GeneratesCorrectRanges_Monthly3()
		{
			var definition = new PlacementDefinition
			{
				Start = new DateTime(2017,01,04),
				End = new DateTime(2017,05,12),
				PlacementType = PlacementType.Monthly
			};

			var result = definition.TimesheetDateRanges;

			Assert.NotNull(result);
			Assert.Equal(5, result.Count());

			Assert.Equal(new DateTime(2017,01,04), result.First().Start);
			Assert.Equal(new DateTime(2017,01,31), result.First().End);

			Assert.Equal(new DateTime(2017,02,01), result.Skip(1).First().Start);
			Assert.Equal(new DateTime(2017,02,28), result.Skip(1).First().End);

			Assert.Equal(new DateTime(2017,03,01), result.Skip(2).First().Start);
			Assert.Equal(new DateTime(2017,03,31), result.Skip(2).First().End);

			Assert.Equal(new DateTime(2017,04,01), result.Skip(3).First().Start);
			Assert.Equal(new DateTime(2017,04,30), result.Skip(3).First().End);

			Assert.Equal(new DateTime(2017,05,01), result.Skip(4).First().Start);
			Assert.Equal(new DateTime(2017,05,12), result.Skip(4).First().End);
		}

		[Fact]
		public void DateRanges_GeneratesCorrectRanges_SingleDay()
		{
			var definition = new PlacementDefinition
			{
				Start = new DateTime(2017,01,04),
				End = new DateTime(2017,01,04),
				PlacementType = PlacementType.Weekly
			};

			var result = definition.TimesheetDateRanges;

			Assert.NotNull(result);
			Assert.Single(result);

			Assert.Equal(new DateTime(2017,01,04), result.First().Start);
			Assert.Equal(new DateTime(2017,01,04), result.First().End);
		}

		[Fact]
		public void DateRanges_GeneratesCorrectRanges_SingleDay_StartAfterEndInSameDay()
		{
			var definition = new PlacementDefinition
			{
				Start = new DateTime(2017,01,04, 12, 30, 00),
				End = new DateTime(2017,01,04, 00, 00, 00),
				PlacementType = PlacementType.Weekly
			};

			var result = definition.TimesheetDateRanges;

			Assert.NotNull(result);
			Assert.Single(result);

			Assert.Equal(new DateTime(2017,01,04), result.First().Start);
			Assert.Equal(new DateTime(2017,01,04), result.First().End);
		}

		[Fact]
		public void DateRanges_GeneratesCorrectRanges_StartAfterEndInDifferentDay()
		{
			var definition = new PlacementDefinition
			{
				Start = new DateTime(2017,01,04),
				End = new DateTime(2017,01,03),
				PlacementType = PlacementType.Weekly
			};

			var result = definition.TimesheetDateRanges;

			Assert.NotNull(result);
			Assert.Empty(result);
        }
	}
}
