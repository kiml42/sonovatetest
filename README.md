This is a solution for the Sonovate Technical Project. It is a web application that generates timesheets based on user input.

To run open the solution in Visual Studio and run IIS Express.
Alternatively navigate to the src\Sonovate and run 'dotnet run' in a terminal.
Either way you can navigate to 'http://localhost:58285/' in a web browser to use the application.

The timesheets can be shown in CSV format. This text can be copied from the web page and pasted into a text file to generate a usable spreadsheet.

The unit tests can be run through Visual Studio, or by navigating to test\SonovateTests and running dotnet test in a terminal.

Ideas for future work
    Storage and loading of placement definitions
    Downloading of timesheets as CSV files, rather than just showing a web page in CSV format
    Autocomplete of names and job titles from previous inputs
    Ability to fill in and save timesheets through the web application itself
