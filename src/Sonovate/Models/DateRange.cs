﻿using System;

namespace Sonovate.Models
{
    /// <summary>
    /// Defines a date range, without time components.
    /// </summary>
    public struct DateRange
    {
        private DateTime _start;
        private DateTime _end;

        /// <summary>
        /// The start of this DateRange
        /// Will always use the Date field of the DateTime only
        /// (time will be zeroed)
        /// </summary>
        public DateTime Start { get { return _start; } set { _start = value.Date; } }

        /// <summary>
        /// The end of this DateRange
        /// Will always use the Date field of the DateTime only
        /// (time will be zeroed)
        /// </summary>
        public DateTime End { get { return _end; } set { _end = value.Date; } }

        public DateRange(DateTime start, DateTime end) : this()
        {
            Start = start;
            End = end;
        }
    }
}
