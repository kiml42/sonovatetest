﻿using Microsoft.AspNetCore.Mvc;
using Sonovate.Models;
using System.Diagnostics;

namespace Sonovate.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult EditPlacement(PlacementDefinition timesheetDef)
        {
            ViewData["Title"] = "Define Placement";
            return View(timesheetDef);
		}

        public IActionResult ViewTimesheet(PlacementDefinition timesheetDef, int id)
        {
            ViewBag.Index = id;
            return View(timesheetDef);
        }

        public IActionResult ViewCsvTimesheet([FromQuery] PlacementDefinition timesheetDef, int id)
        {
            ViewBag.Index = id;
            return View(timesheetDef);
        }

        public IActionResult Contact()
		{
			ViewData["Message"] = "Your contact page.";

			return View();
		}

		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}
	}
}
