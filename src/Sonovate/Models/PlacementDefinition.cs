using System;
using System.Collections.Generic;
using static Sonovate.Models.PlacementType;

namespace Sonovate.Models
{
    public class PlacementDefinition
    {
        public string CandidateName { get; set; }
        public string ClientName { get; set; }
        public string JobTitle { get; set; }

        private DateTime? _start;
        private DateTime? _end;

        /// <summary>
        /// The start of this TimeSheet
        /// Will always use the Date field of the DateTime only
        /// (time will be zeroed)
        /// </summary>
        public DateTime? Start { get { return _start; } set { _start = value?.Date; } }

        /// <summary>
        /// The end of this TimeSheet
        /// Will always use the Date field of the DateTime only
        /// (time will be zeroed)
        /// </summary>
        public DateTime? End { get { return _end; } set { _end = value?.Date; } }

        public PlacementType PlacementType { get; set; }

        public List<DateRange> TimesheetDateRanges
        {
            get
            {
                if (!End.HasValue || !Start.HasValue || End < Start) return new List<DateRange>();

                DateRange? current = null;
                var result = new List<DateRange>();

                do
                {
                    current = GetRangeForStartDate(current?.End.AddDays(1) ?? Start.Value);
                    result.Add(current.Value);
                }
                while (current.Value.End < End.Value.Date);
                
                return result;
            }
        }

        private DateRange GetRangeForStartDate(DateTime date)
        {
            return new DateRange(date.Date, GetRangeEndForStartDate(date));
        }

        private DateTime GetRangeEndForStartDate(DateTime date)
        {
            if (!End.HasValue) throw new ArgumentException("Cannot get end date for a date range if the end date for the Placement is null.");
            DateTime endOfPeriod;
            switch (PlacementType)
            {
                case Weekly:
                    endOfPeriod = GeWeekEndForStartDate(date);
                    break;
                case Monthly:
                    endOfPeriod = GeMonthEndForStartDate(date);
                    break;
                default:
                    throw new ArgumentOutOfRangeException($"Invalid PlacementType {PlacementType}.");
            }
            return endOfPeriod < End.Value ? endOfPeriod : End.Value;
        }

        private DateTime GeWeekEndForStartDate(DateTime date)
        {
            var dayOfWeekNumber = (int)date.DayOfWeek;
            var daysToAdd = (7 - dayOfWeekNumber) % 7;
            return date.AddDays(daysToAdd);
        }

        private DateTime GeMonthEndForStartDate(DateTime date)
        {
            var nextMonth = date.AddMonths(1);
            return new DateTime(nextMonth.Year, nextMonth.Month, 1).AddDays(-1);
        }

        //private bool NewRange(DateTime date)
        //{
        //    switch (PlacementType)
        //    {
        //        case Weekly:
        //            return date.DayOfWeek == DayOfWeek.Monday;
        //        case Monthly:
        //            return date.Day == 1;
        //        default:
        //            throw new ArgumentOutOfRangeException($"Invalid PlacementType {PlacementType}.");
        //    }
        //}
    }
}